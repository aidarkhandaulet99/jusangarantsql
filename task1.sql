SELECT c.name, m.name
FROM customers c
         JOIN managers m ON m.id = c.manager_id
         JOIN orders o ON c.id = o.customer_id
WHERE o.amount > 10000
  AND o.date >= '2013.01.01';
