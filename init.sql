create table MANAGERS
(
    id serial not null,
    name varchar(100)
);

create unique index managers_id_uindex
	on MANAGERS (id);

alter table MANAGERS
    add constraint managers_pk
        primary key (id);



create table CUSTOMERS
(
    id serial not null,
    name varchar(100),
    manager_id int,
    foreign key (manager_id) references managers(id)
);

create unique index customers_id_uindex
    on CUSTOMERS (id);

alter table CUSTOMERS
    add constraint customers_pk
        primary key (id);



create table ORDERS
(
    id serial not null,
    date date,
    amount int,
    customer_id int,
    foreign key (customer_id) references customers(id)
);

create unique index orders_id_uindex
	on ORDERS (id);

alter table ORDERS
    add constraint orders_pk
        primary key (id);



create table employees
(
    emp_id serial not null,
    emp_name varchar(100),
    dept_id int,
    salary int
);

create unique index employees_emp_id_uindex
	on employees (emp_id);

alter table employees
    add constraint employees_pk
        primary key (emp_id);
