SELECT *
FROM employees
WHERE emp_name LIKE '%m%'
   OR emp_name LIKE '%M%';

SELECT emp_name,
       dept_id,
       salary
FROM employees
WHERE (dept_id, salary)
          IN
      (SELECT dept_id, max(salary) FROM employees GROUP BY dept_id);

SELECT *
FROM employees
WHERE salary IN
      (SELECT salary
       FROM employees e
       WHERE employees.emp_id <> e.emp_id);

